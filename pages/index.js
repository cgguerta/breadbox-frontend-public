import Head from 'next/head'
import Banner from '../components/Banner'
import Navbar from '../components/NavBar'

export default function Home() {

  
  return (
    <React.Fragment>
      <Head>
        <title>BreadBox</title>
      </Head>
      <Banner />
    </React.Fragment>
  )
}