import { useEffect, useState, useContext } from 'react'
import { Tabs, Tab, Container, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import moment from 'moment'
import React from 'react';
import ReactDOM from 'react-dom';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { Line, Doughnut} from 'react-chartjs-2';
// import DoughnutCategories from '../components/DoughnutCategories'
import styled from 'styled-components';



import UserContext from '../UserContext'

const TrendContainer = styled(Container)`
    background: #ffeadb;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
    display: flex;
    align-items: center;
    margin-bottom: 2vh;
`

export default function history(){
	const { user } = useContext(UserContext)

	const[lineData, setLineData] = useState({})
	const[doughnutData, setDoughnutData] = useState({})
	const[incomeDoughnutData, setIncomeDoughnutData] = useState({})
	const[expenseDoughnutData, setExpenseDoughnutData] = useState({})

    const [incomePerCat, setIncomePerCat] = useState({})
    const [expensePerCat, setExpensePerCat] = useState({})


	const[month, setMonth] = useState('')




useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
	})
	.then(res => res.json())
    .then(data => {
        	if(data._id){
        		console.log(data)
        		//Weekly Report
        		let income = [0, 0, 0, 0, 0, 0, 0]
        		let expense = [0, 0, 0, 0, 0, 0, 0]
        		data.cashFlow.forEach(transaction => {
        			if(transaction.transaction == 'income'){
        				income[moment(transaction.date).day()] += parseInt(transaction.amount)
        			} else {
        				expense[moment(transaction.date).day()] += parseInt(transaction.amount)
        			}
        		})
        		//Income V Expense Doughnut
        		let incomeD = 0
        		let expenseD = 0
                let incomeArr = []
                let expenseArr = []
        		data.cashFlow.forEach(transaction => {
        			if(transaction.transaction == 'income'){
        				incomeD += parseInt(transaction.amount)
        			} else {
        				expenseD += parseInt(transaction.amount)
        			}
        		})
                data.cashFlow.forEach(data => {
                    if(data.transaction == 'income'){
                        if(!incomeArr.includes(data.category.name)){
                            incomeArr.push(data.category.name)
                        }
                    } else {
                        if(!expenseArr.includes(data.category.name)){
                            expenseArr.push(data.category.name)}
                    }
                })

        		//Category Doughnuts
                const incomeDoughnutDataPerCat = incomeArr.map(category => {
                    let amount = 0
                    data.cashFlow.forEach(transaction => {
                        if(category == transaction.category.name){
                            amount += parseInt(transaction.amount)
                        }
                    }) 
                    return amount
                })
                const expenseDoughnutDataPerCat = expenseArr.map(category => {
                    let amount = 0
                    data.cashFlow.forEach(transaction => {
                        if(category == transaction.category.name){
                            amount += parseInt(transaction.amount)
                        }
                    }) 
                    return amount
                })
                setIncomePerCat({

                    labels: incomeArr,
                    datasets: [{
                        label: "Income",
                        data: incomeDoughnutDataPerCat,
                        backgroundColor: incomeDoughnutDataPerCat.map(data => {
                            return `rgb(${Math.floor(Math.random()*255)} , ${Math.floor(Math.random()*255)}, ${Math.floor(Math.random()*255)}`
                            }),
                        borderColor: 'rgba(54, 54, 54, .5)'
                        }],
                })
                setExpensePerCat({
                    labels: expenseArr,
                    datasets: [{
                        label: "Expense",
                        data: expenseDoughnutDataPerCat,
                        backgroundColor: expenseDoughnutDataPerCat.map(data => {
                            return `rgb(${Math.floor(Math.random()*255)} , ${Math.floor(Math.random()*255)}, ${Math.floor(Math.random()*255)}`
                        }),
                        borderColor: 'rgba(54, 54, 54, .5)'
                    }],
                })

                console.log(incomeDoughnutDataPerCat)
                console.log(expenseDoughnutDataPerCat)
                console.log(incomeArr)
                console.log(expenseArr)


        		setLineData({
        			labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
        			datasets: [
        			{
        				label: 'Income',
        				data: income,
                        backgroundColor: '#679b9b',
                        fill: false,
                        borderColor: '#679b9b'
        			},
        			{
						label: 'Expense',
						data: expense,
                        backgroundColor: '#ff9a76',
                        fill: false,
                        borderColor: '#ff9a76'
        			}]
        		})
        		setDoughnutData({
        			labels: [
							'Income',
							'Expenses'
							],
						datasets: [{
							data: [incomeD, expenseD],
							backgroundColor: [
							'#679b9b',
							'#ff9a76'
							],
							hoverBackgroundColor: [
							'#679b9b',
							'#ff9a76'
							],
                            borderColor: 'rgba(54, 54, 54, .5)'
						}]
        		})
         	} else {
        		
        	}
        })
	}, [])

return (
<React.Fragment>
<Row className="mx-1">
<Col>
<h3 className="subtitle">Weekly Report</h3>
 <TrendContainer>
				<Line data={lineData}/>
</TrendContainer>
</Col>
<Col>
<h3 className="subtitle">Income VS Expenses</h3>
<TrendContainer>
				<Doughnut data={doughnutData}/>
</TrendContainer>
</Col>
</Row>
<Row className="mx-1">
<Col>
<h3 className="subtitle">Income Breakdown</h3>
<TrendContainer>
                <Doughnut data={incomePerCat}/>
</TrendContainer>
</Col>
<Col>
<h3 className="subtitle">Expenses Breakdown</h3>
<TrendContainer>
                <Doughnut data={expensePerCat}/>
</TrendContainer>
</Col>
</Row>
</React.Fragment>
)
}