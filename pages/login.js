import { useState, useContext } from 'react'
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper'
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

import Router from 'next/router'
import Head from 'next/head'

const LoginContainer = styled(Container)`
    background: #ffeadb;
    height: 60vh;
    width: 30vw;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
    padding: 1em;
    display: flex;
    align-items: center;
    flex-direction: column;

`
const LoginButt = styled(Button)`
    color: #ffeadb;
    background-color: #679b9b;
    font-size: 1em;
    margin: .25em;
    padding: 0.25em 1em;
    border: transaprent;
    border-radius: 5px;
    font-family: Be Vietnam
`

export default function login() {
    const { setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    function authenticate(e) {

        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            
            if(data.accessToken){
                
                localStorage.setItem('token', data.accessToken);
                
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
                    
                    setUser({
                        id: data._id
                    })
                    Router.push('/dashboard')
                })
            }else{
                if(data.error === 'does-not-exist') {
                    Swal.fire(
                        'Authentication Failed',
                        'User does not exist.',
                        'error'
                    )
                } else if (data.error === 'incorrect-password') {
                    Swal.fire(
                        'Authentication Failed',
                        'Password is incorrect.',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }
            }
        })
    }
    const authenticateGoogleToken = (response) => {
        console.log(response)

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data)
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { 
                Authorization: `Bearer ${ accessToken }`
            }
        }
        console.log(`${ AppHelper.API_URL }/users/details`)

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

            Router.push('/dashboard')
        })
    }

    return (
        <React.Fragment>
            <Head>
                <title>Login</title>
            </Head>
            <LoginContainer>
            <h3><strong>sign in/up to enter breadbox:</strong></h3>
            <Col>
            
            <Form onSubmit={(e) => authenticate(e)}>
{/*                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>*/}
            <Row>
                    <TextField 
                    id="standard-basic"
                    label="Email Address:"
                    type="string"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    />
            </Row>
            <Row>
{/*                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>*/}

                    <TextField
                    className="mb-3" 
                    id="standard-basic"
                    label="Password:"
                    type="password"
                    placeholder="Enter email"
                    placeholder="Password" value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                    required
                    />
            </Row>
                <LoginButt variant="light" type="submit">
                    Sign In
                </LoginButt>
                <LoginButt variant="light" className="ml-1" onClick={()=>Router.push('/register')}>
                    Sign Up
                </LoginButt>
            </Form>
            </Col>
                <GoogleLogin
                            clientId="573392503079-hdcch9ef198th4fkovvpd3b0u5n1dbtd.apps.googleusercontent.com"
                            buttonText="Sign In Via Google"
                            onSuccess={ authenticateGoogleToken }
                            onFailure={ authenticateGoogleToken }
                            cookiePolicy={ 'single_host_origin' }
                            className="w-30 mt-1 text-center d-flex justify-content-left"
                        />
            </LoginContainer>
        </React.Fragment>
    )
}