import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { UserProvider } from '../UserContext'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'


import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'

export default function MyApp({ Component, pageProps }) {
    const [user, setUser] = useState({
        id: null
    })

    useEffect(() => {

        const token = localStorage.getItem('token')

        if ( token != null ) {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
                setUser({id: data._id})
            })
        .catch(error => console.log(error))
        }
    }, [user.id])

    const unsetUser = () => {
        localStorage.clear()

        setUser({
            id: null
        });
    }



  return (
        <React.Fragment>
  		<UserProvider value={{user, setUser, unsetUser}}>
	  		<NavBar/>
  	<Component {...pageProps} />
  	</UserProvider>
    {/*<Footer className="footer-main"/>*/}
    </React.Fragment>
  	)
}

