import { useState, useEffect, useContext } from 'react'
import { Form, Card, Row, Col, ListGroup, Container, Button } from 'react-bootstrap'
import Router from 'next/router'
import Head from 'next/head'
import Swal from 'sweetalert2';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';


import UserContext from '../UserContext'
    
const AccountContainer = styled(Container)`
    background: #ffeadb;
    width: 40vw;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
    display: flex;
    align-items: center;
    margin-bottom: 2vh;
`

const AccountsButt = styled(Button)`
    color: #ffeadb;
    background-color: #679b9b;
    font-size: 1em;
    margin: .25em;
    padding: 0.25em;
    border: transaprent;
    border-radius: 5px;
    font-family: Be Vietnam
`

export default function myaccount() {

    const { user } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [loginType, setLoginType] = useState('')
    const [newFirstName, setNewFirstName] = useState('')
    const [newLastName, setNewLastName] = useState('')
    const [newMobileNo, setNewMobileNo] = useState('')
    const [newPassword1, setNewPassword1] = useState('')
    const [newPassword2, setNewPassword2] = useState('')
    const [categoryName, setCategoryName] = useState('')

useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setFirstName(data.firstName)
            setLastName(data.lastName)
            setMobileNo(data.mobileNo)
            setLoginType(data.loginType)
        })
    },[])

function editAccount(e) {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
                _id: user.id,
				firstName: newFirstName,
				lastName: newLastName,
				mobileNo: newMobileNo,
				password: newPassword1,

			})
		})
		.then(res => res.json())
		.then(data => {
            console.log(data)
			// if update course successful
			if(data === true){
				Swal.fire(
                        'Nice!',
                        'Your profiile is updated!',
                        'success'
                        )
			}else{
				// error in editing page
				Router.push('/error')
			}
            // if((newPassword1 !== '' && newPassword2 !== '') && (newPassword2 === newPassword1)){
            // setIsActive(true)
            // }else{
            // setIsActive(false)
            // }

		})
	}
function addCategory(e) {
    e.preventDefault()
    console.log(categoryName)

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/category/category-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: categoryName,
            })
        })
        .then(res => res.json())
        .then(data => { 
            if(data === false){
            fetch(`${process.env.NEXT_PUBLIC_API_URL}/category`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name: categoryName,
                    })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                // if update course successful
                if(data === true){
                        Swal.fire(
                        'Nice!',
                        'Category Saved!',
                        'success'
                        )
                }else{
                    // error in editing page
                         Router.push('/error')
                    }
            })
            }else {
                Swal.fire(
                'Alert!',
                'Category already exists!',
                'error'
                )
            }
            console.log(data)
        })
}


    return (
        <React.Fragment>
            <Head>
                <title>BreadBox-My Account</title>
            </Head>
        <Container className="d-flex">
        <Row>
        <Col>
                <h3 className="subtitle">My Profile:</h3>
                <AccountContainer className="mx-1">
                    <Card.Body>
                        <Card.Text><strong>Name:</strong> {firstName} {lastName}</Card.Text>
                        <Card.Text><strong>Mobile Number:</strong> {mobileNo}</Card.Text>
                        </Card.Body>
                        <div className="ml-3">
                        <h4>Add a category:</h4>
                        <form onSubmit={(e) => addCategory(e)}>
                        <TextField 
                        id="category"
                        label="Category"
                        placeholder="add your own category"
                        value={categoryName} 
                        onChange={e => setCategoryName(e.target.value)}
                        />
                        <AccountsButt variant="outlined" color="primary" type="submit" id="submitBtn2">Add Category</AccountsButt>
                        </form>
                        </div>
                </AccountContainer>

        </Col>
        </Row>
        <Row>
            <Col>
                <h3 className="subtitle">Update your account:</h3>
                <AccountContainer>
                <Card.Body>
                <form onSubmit={(e) => editAccount(e)}>
                    <Row>  
                    <div className="ml-3">
                    <TextField 
                    id="firstName"
                    label="First Name"
                    placeholder="enter updated first name"
                    value={newFirstName} 
                    onChange={e => setNewFirstName(e.target.value)}
                    />
                    </div>

                    <div className="ml-3">
                    <TextField 
                    id="lastName"
                    label="Last Name"
                    placeholder="enter updated last name"
                    value={newLastName} 
                    onChange={e => setNewLastName(e.target.value)}
                    />
                    </div>
                    <div className="ml-3">
                    <TextField 
                    id="mobileNo"
                    label="Mobile No.:"
                    placeholder="enter updated 11-digit mobile number"
                    value={newMobileNo} 
                    onChange={e => setNewMobileNo(e.target.value)}
                    />
                    </div>
                    </Row>
                    <Row>
                {loginType === "email"
                ?
                <React.Fragment>

                    <div className="ml-3">
                    <TextField 
                    id="password1"
                    type="password"
                    label="Password"
                    placeholder="enter new password"
                    value={newPassword1} 
                    onChange={e => setNewPassword1(e.target.value)}
                    required
                    />
                    </div>
                    <div className="ml-3">
                   <TextField 
                    id="password2"
                    type="password"
                    label="Verify Password"
                    placeholder="verify new password"
                    value={newPassword2} 
                    onChange={e => setNewPassword2(e.target.value)}
                    required
                    />
                    </div>

                </React.Fragment>
                :
                null }
                </Row>
                {/* conditionally render submit button based on isActive state */}
                <div className="my-3">
                <AccountsButt variant="outlined" color="primary" type="submit" id="submitBtn1">Update Profile</AccountsButt>
                </div>
            </form>
            </Card.Body>
            </AccountContainer>
            </Col>
            </Row>
        </Container>
        </React.Fragment>
    )

}
/*
                 {isActive
                    ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                }*/

