import { useEffect, useRef, useState, useContext } from 'react'
import { Row, Col, Card, Form, Alert, Container, Button } from 'react-bootstrap'
import Router from 'next/router'
import Head from 'next/head'
import dynamic from 'next/dynamic'
import Swal from 'sweetalert2';
import React from 'react';
import ReactDOM from 'react-dom';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';



const DashboardContainer = styled(Container)`
    background: #ffeadb;
    height: 30vh;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
    padding: 1em;
    display: flex;
    align-items: center;
    flex-direction: column;
    margin-bottom: 2vh;
`
const DashboardContainer2 = styled(Container)`
    background: #ffeadb;
    height: 40vh;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
    padding: 1em;
    display: flex;
    align-items: center;
    flex-direction: column;
    margin-bottom: 2vh;
`

const TransactButtIn = styled(Button)`
    color: #ffeadb;
    background-color: #679b9b;
    font-size: 1em;
    margin: .25em;
    padding: 0.25em 1em;
    width: 5em;
    border: transaprent;
    border-radius: 5px;
    font-family: Be Vietnam
`
const TransactButtOut = styled(Button)`
    color: #ffeadb;
    background-color: #ff9a76;
    font-size: 1em;
    margin: .25em;
    padding: 0.25em 1em;
    width: 5em;
    border: transaprent;
    border-radius: 5px;
    font-family: Be Vietnam
`



import UserContext from '../UserContext'

const DynamicComponent = dynamic(() => import('../components/History'))
const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
          margin: theme.spacing(3),
          flexGrow: 1,
        },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        },
  },
}));


export default function Dashboard() {
	const { user } = useContext(UserContext)
	// const state = {button: 1}

	const[firstName,setFirstName] = useState('')
	const[transaction,setTransaction] = useState('')
	const[amount, setAmount] = useState()
	const[description, setDescription] = useState('')
	const[wallet, setWallet] = useState(0)
    const[categoriesArray, setCategoriesArray] = useState([])
    const[category, setCategory] = useState('')
    const classes = useStyles();
    const[date, setDate]= useState(new Date())



useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
                    fetch(`${process.env.NEXT_PUBLIC_API_URL}/category/allcats`, {
                    method: 'GET',
                    headers: {
                    'Content-Type': 'application/json',
                    }
                    })
                    .then(res =>res.json())
                    .then(data => {
                    console.log(data)
                    setCategoriesArray(data)
                    })
            console.log(data)
            console.log(wallet)
            setWallet(data.wallet)
            setFirstName(data.firstName)
        })
    },[])

function addTransact(e){
	fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/transact`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                amount: amount,
                date: date,
                description: description,
                transaction: transaction,
                category: {
                    name: category,
                }
            })
        })
		.then(res => res.json())
		.then(data => {
        	if(transaction === 'income'){
                Swal.fire(
                'Gettin Paid!',
                'Income saved!',
                'success'
                )
            console.log(data)
        	} else if(transaction === 'expense'){
                Swal.fire(
                'Making it rain!',
                'Expense saved!',
                'success'
                )
            console.log(data)
            } 
            else {
        		Router.push('/error')
        	}
        })
}

    return (
    <React.Fragment>
    <div className="mx-auto">
       <DashboardContainer>
       <h2 className="welcome">Welcome {firstName}! </h2>
       <h1 className="your-bread">Your Bread: {wallet}Php</h1>
    <form className={classes.root} onSubmit={(e) => addTransact(e)}>
        <Grid container spacing={3}>
            {/*<Grid item xs={12}>*/} 
            <TextField 
            id="amt"
            label="Amount"
            type="number"
            multiline
            rowsMax={4}
            value={amount}
            onChange={e => setAmount(e.target.value)}
            />
            <div className="ml-3">
            <TextField 
            id="desc"
            label="Description"
            multiline
            rowsMax={4}
            value={description}
            onChange={e => setDescription(e.target.value)}
            />
            </div>
            <div className="ml-3">
            <TextField
              id="useCategory"
              select
              label="Category"
              onChange={e => setCategory(e.target.value)}
              helperText="Please select your category"
            >
              {categoriesArray.map((cats) => (
                <MenuItem key={cats.name} value={cats.name}>
                  {cats.name}
                </MenuItem>
              ))}
            </TextField>
            </div>
            <div className="ml-3">
            <TextField
                id="date"
                label="Date of Transaction"
                type="date"
                defaultValue={new Date()}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={e => setDate(e.target.value)}
              />
            </div>
          <TransactButtIn variant="outlined" color="primary" type="submit" onClick={() => setTransaction('income')} name="btn" value="gettin_paid">
            In
          </TransactButtIn>
          <TransactButtOut variant="outlined" color="primary" type="submit" onClick={() => setTransaction('expense')} name="btn" value="make_it_rain">
            Out
          </TransactButtOut>
        </Grid>
    </form>
    </DashboardContainer>
    <DashboardContainer2>
    <h1 className="transactions">Transactions:</h1>
    <Grid>
    <Grid className="mx-auto my-1">
    <DynamicComponent className="history-table" />
    </Grid>
    </Grid>
    </DashboardContainer2>
    </div>
	</React.Fragment>
    )
}



{/*                <Form.Group controlId="userCategory">
                <Form.Label>Category</Form.Label>
                <Form.Control as="select" value={category} onChange={e => setCategory(e.target.value)}>
                    {categoriesArray.map(cats =>{
                        return(
                            <option key={cats.name}>{cats.name}</option>
                        )
                    })}
                </Form.Control>
                </Form.Group>*/}