import { useState, useEffect } from 'react'
import { Form, Button, Container } from 'react-bootstrap'
import Router from 'next/router'
import Head from 'next/head'
import styled from 'styled-components';

const RegisterContainer = styled(Container)`
    background: #ffeadb;
    height: 70vh;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
`

export default function register() {
    
    const [email, setEmail] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [wallet, setWallet] = useState(0)
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        console.log(`${process.env.NEXT_PUBLIC_API_URL}/users/email-exists`)

        if((password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }

    }, [password1, password2])

    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data === false){
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        password: password1,
                        wallet: wallet
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data === true){
                        Router.push('/login')
                    }else{
                        Router.push('/error')
                    }
                })
            }else{
                Router.push('/error')
            }
        })
    } 

    return (
        <React.Fragment>
            <Head>
                <title>Register</title>
            </Head>
            <RegisterContainer>
            <form onSubmit={(e) => registerUser(e)}>

                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                 <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="" placeholder="enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
                </Form.Group>

                 <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="" placeholder="enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="mobileNo">
                    <Form.Label>Mobile No</Form.Label>
                    <Form.Control type="number" placeholder="11-digit Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
                </Form.Group>
                {/* conditionally render submit button based on isActive state */}
                {isActive
                    ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                }
                
            </form>
            </RegisterContainer>
        </React.Fragment>
    )
}