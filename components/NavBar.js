import { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'
import styled from 'styled-components';

import UserContext from '../UserContext'

export default function NavBar() {
    const { user } = useContext(UserContext)

    return (
        <Navbar expand="lg">
            <Link href="/">
                <a className="navbar-brand">
                <img src="https://i.imgur.com/DoGnudj.png"/>
                </a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                <Nav className="mr-auto">
                    
                    {(user.id !== null)
                        ? <React.Fragment>
                            <Link href="/dashboard">
                                <a className="nav-link" role="button">dashboard</a>
                            </Link>
                            <Link href="/trends">
                                <a className="nav-link" role="button">trends</a>
                            </Link>
                            <Link href="/myaccount">
                                <a className="nav-link" role="button">my account</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">logout</a>
                            </Link>
                        </React.Fragment>
                        : 
                        null
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}