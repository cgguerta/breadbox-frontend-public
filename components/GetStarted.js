import Router from 'next/router'
import { Button } from 'react-bootstrap'
import styled from 'styled-components';

const GetStartedButt = styled(Button)`
	color: #ffeadb;
	background-color: #679b9b;
	font-size: 1.5em;
	margin: .25em;
	padding: 0.25em 1em;
	border: transaprent;
	border-radius: 5px;
	font-family: Be Vietnam
`

export default function GetStarted() {
    return (
        <GetStartedButt variant="light" onClick={()=>Router.push('/login')}>Get Started!</GetStartedButt>
    )
}