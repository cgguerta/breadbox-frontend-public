import { useContext } from 'react'
import Link from 'next/link'
import { Form, Card, Row, Col, ListGroup, Container, Button } from 'react-bootstrap'
import styled from 'styled-components';

const FootContainer = styled(Container)`
	color: white;
	background-color:#679b9b;
	position: relative;
	bottom:0;
	width: 100%;

`

export default function NavBar() {
	return(
		<FootContainer>
		<div className="main-footer">
			<div className="container">
			<Row className="row">
			<p>&copy;2020 By:CQG</p>
			<p className="ml-3">All Rights Reserved</p>
			</Row>
			</div>
		</div>
		</FootContainer>

	)
}