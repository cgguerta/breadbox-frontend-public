import { Row, Col, Container } from 'react-bootstrap'
// import BackButton from './BackButton'
import GetStarted from './GetStarted'
import { useRouter } from 'next/router'
import styled from 'styled-components';


const StyledContainer = styled(Container)`
    background-image: url(https://images.pexels.com/photos/3801422/pexels-photo-3801422.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260);
    background-size: cover;
    height: 60vh;
    border-radius: 20px;
    font-family: Be Vietnam;
    font-size: 20px;
    box-shadow: 5px 10px 15px #888888;
`

const StyledCol= styled(Col)`
    align-items: center;
    margin-bottom: 5vh;
    margin-top: 5vh;
`

const StyledCol2= styled(Col)`
    align-items: center;
    margin-bottom: 5vh;
    margin-top: 5vh;
    width; 5vw;
`
export default function Banner({data}) {
    const router = useRouter()
    return (
        <React.Fragment>
        <Row>
            <Col>
                <StyledContainer >
                    <Row>
                    <StyledCol>

                    <img src="https://i.imgur.com/f1bcloY.png"/>
                    <p>Keeping your budget safe from spoiling!</p>
                    <GetStarted/>

                    </StyledCol>
                    </Row>
                </StyledContainer>
            </Col>
        </Row>
        </React.Fragment>
    )
}