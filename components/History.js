import { useEffect, useRef, useState, useContext } from 'react'
import { Alert } from 'react-bootstrap'
import moment from 'moment'
import Head from 'next/head'
import UserContext from '../UserContext'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TableScrollbar from 'react-table-scrollbar';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


const History = () => {
	const[records, setRecords] = useState([])
	const[transaction,setTransaction] = useState('')
	const[category, setCategory] = useState('')
	const[amount, setAmount] = useState()
	const[description, setDescription] = useState('')
	const[wallet, setWallet] = useState(0)
	
	const[result, setResult] = useState('')
	const[searchResult, setSearchResult] = useState([])
	const[transactionQuery, setTransactionQuery] = useState('')
	const classes = useStyles();

useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
	})
	.then(res => res.json())
    .then(data => {
        	if(data._id){
        		setRecords(data.cashFlow)
        		setWallet(data.wallet)
        	} else {
        		setRecords([])
        	}
        })
	}, [])

const filter = e => {
	setTransactionQuery(e.target.value)
	filterTransact()
}
const filterTransact = () => {
	const filterItem = records.filter(record => record.description.toLowerCase().includes(transactionQuery))
	// console.log(filterItem)
	setSearchResult(filterItem)
}

useEffect(() => {
		filterTransact()
	}, [transactionQuery])

const full = searchResult.map(data => {
	return (						
			<TableRow key={data._id}>
				<TableCell align="right">{data.amount}</TableCell>
				<TableCell align="right">{data.description}</TableCell>
				<TableCell align="right">{data.category.name}</TableCell>
				<TableCell align="right">{moment(data.date).format('MMMM DD YYYY')}</TableCell>
			</TableRow>
			)
})

const filtered = records.map(record => {
		return (
			<TableRow key={record._id}>
				<TableCell align="right">{record.amount}</TableCell>
				<TableCell align="right">{record.description}</TableCell>
				<TableCell align="right">{record.category.name}</TableCell>
				<TableCell align="right">{moment(record.date).format('MMMM DD YYYY')}</TableCell>
			</TableRow>
			)
	})

return (
	<React.Fragment>
		<div className="history-table">
		<div className="mb-1">
		<TextField label="Search" type="text" placeholder="filter descriptions" value={transactionQuery} onChange={filter}/>
		</div>
		{records.length > 0
				?
				<TableContainer component={Paper}>
					<TableScrollbar rows={6}>
					<Table stickyHeader className={classes.table} aria-label="a dense table" fixed>
						<TableHead>
							<TableRow>
								<TableCell align="right">Amount</TableCell>
								<TableCell align="right">Description</TableCell>
								<TableCell align="right">Category</TableCell>
								<TableCell align="right">Date</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{(transactionQuery == '')
								?
								filtered
								:
								full	
								}	
						</TableBody>
					</Table>
					</TableScrollbar>
				</TableContainer>
				:
				<Alert variant="info">You have no transaction records yet.</Alert>
				}
		</div>
	</React.Fragment>
)
}

export default History
